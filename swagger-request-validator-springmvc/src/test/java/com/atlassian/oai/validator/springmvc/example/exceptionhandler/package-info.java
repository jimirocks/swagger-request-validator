/**
 * This example extends the {@link com.atlassian.oai.validator.springmvc.example.simple} example by an global
 * exception handler mapping the {@link com.atlassian.oai.validator.springmvc.InvalidRequestException} to a
 * custom error response.
 *
 * @see com.atlassian.oai.validator.springmvc.example.exceptionhandler.RestServiceExceptionHandler
 */
package com.atlassian.oai.validator.springmvc.example.exceptionhandler;